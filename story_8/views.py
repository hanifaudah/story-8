# Django imports
from django.shortcuts import render, redirect
from django.http import JsonResponse

# Other imports
import requests
import json

# constants
API_URL = 'https://www.googleapis.com/books/v1/volumes?q='

def index(request):
  return render(request, 'story_8/index.html')

def search(request, keyword=None):
  if keyword:
    response = requests.get(f'{API_URL}{keyword}')
    return JsonResponse({
      "status_code": response.status_code,
      "data": response.json()
    })

  else:
    return redirect('home')

