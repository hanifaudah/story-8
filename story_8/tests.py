from django.test import TestCase, Client
from django.http import HttpRequest

# views
from story_8.views import index, search

class SmokeTest(TestCase):
  def test_does_pipeline_work(self):
    return self.assertEquals(1, 1)

class TestUrls(TestCase):
  def test_index_url_exists(self):
    response = Client().get('')
    self.assertEquals(response.status_code, 200)

  def test_index_template(self):
    response = Client().get('')
    self.assertTemplateUsed(response, 'story_8/index.html')

  def test_search_url_exists(self):
    response = Client().get('/search/')
    self.assertEquals(response.status_code, 302)

  def test_search_url_with_params(self):
    response = Client().get('/search/hello')
    self.assertEquals(response.status_code, 200)

class TestViews(TestCase):
  def test_index_view_response(self):
    response = index(HttpRequest())
    self.assertEquals(response.status_code, 200)

  def test_index_view_template(self):
    response = index(HttpRequest())
    html_response = response.content.decode('utf8')
    self.assertIn('Cool Books', html_response)

  def test_search_view_without_keyword(self):
    response = search(HttpRequest())
    self.assertEquals(response.status_code, 302)

  def test_search_view_with_keyword(self):
    response = search(HttpRequest(), 'keyword')
    self.assertEquals(response.status_code, 200)